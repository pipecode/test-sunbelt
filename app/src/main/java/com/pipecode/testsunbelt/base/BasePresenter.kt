package com.pipecode.testsunbelt.base

import com.pipecode.testsunbelt.injection.components.DaggerPresenterInjector
import com.pipecode.testsunbelt.injection.components.PresenterInjector
import com.pipecode.testsunbelt.injection.modules.ContextModule
import com.pipecode.testsunbelt.injection.modules.NetworkModule
import com.pipecode.testsunbelt.ui.movies.presenter.MoviesPresenter

abstract class BasePresenter<out V : BaseView>(protected val view: V) {
    private val injector: PresenterInjector = DaggerPresenterInjector
        .builder()
        .baseView(view)
        .contextModule(ContextModule)
        .networkModule(NetworkModule)
        .build()

    init {
        inject()
    }

    open fun onViewCreated() {}

    open fun onViewDestroyed() {}

    private fun inject() {
        when (this) {
            is MoviesPresenter -> injector.inject(this)
        }
    }
}