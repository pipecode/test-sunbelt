package com.pipecode.testsunbelt.base

import android.content.Context


interface BaseView {

    fun getContext(): Context
}
