package com.pipecode.testsunbelt.injection.components

import com.pipecode.testsunbelt.base.BaseView
import com.pipecode.testsunbelt.injection.modules.ContextModule
import com.pipecode.testsunbelt.injection.modules.NetworkModule
import com.pipecode.testsunbelt.ui.movies.presenter.MoviesPresenter
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [(ContextModule::class), (NetworkModule::class)])
interface PresenterInjector {

    fun inject(moviesPresenter: MoviesPresenter)

    @Component.Builder
    interface Builder {
        fun build(): PresenterInjector

        fun networkModule(networkModule: NetworkModule): Builder
        fun contextModule(contextModule: ContextModule): Builder

        @BindsInstance
        fun baseView(baseView: BaseView): Builder
    }
}