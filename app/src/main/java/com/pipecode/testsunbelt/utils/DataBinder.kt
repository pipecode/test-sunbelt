package com.pipecode.testsunbelt.utils

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.pipecode.testsunbelt.ui.movies.adapters.MoviesAdapter

@BindingAdapter("adapter")
fun setAdapter(view: RecyclerView, adapter: MoviesAdapter) {
    view.adapter = adapter
}

@BindingAdapter("layoutManager")
fun setLayoutManager(view: RecyclerView, layoutManager: RecyclerView.LayoutManager) {
    view.layoutManager = layoutManager
}

@BindingAdapter("dividerItemDecoration")
fun setDividerItemDecoration(view: RecyclerView, dividerItemDecoration: DividerItemDecoration) {
    view.addItemDecoration(dividerItemDecoration)
}

@BindingAdapter("imageUrl")
fun loadImage(
    imageView: ImageView,
    imageUrl: String?
) {
    Glide.with(imageView.context)
        .load(BASE_URL_IMG + imageUrl)
        .centerCrop()
        .into(imageView)

}