package com.pipecode.testsunbelt.utils

const val BASE_URL: String = "https://api.themoviedb.org/3/movie/"
const val BASE_URL_IMG: String = "https://image.tmdb.org/t/p/w500/"