package com.pipecode.testsunbelt.ui.movies

import androidx.annotation.StringRes
import com.pipecode.testsunbelt.base.BaseView
import com.pipecode.testsunbelt.data.local.Movie

interface MovieView : BaseView {

    fun updateMovies(movies: List<Movie>)

    fun showError(error: String)

    fun showError(@StringRes errorResId: Int) {
        this.showError(getContext().getString(errorResId))
    }

    fun showLoading()

    fun hideLoading()
}