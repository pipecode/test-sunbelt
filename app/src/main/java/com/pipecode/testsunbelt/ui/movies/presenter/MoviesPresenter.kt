package com.pipecode.testsunbelt.ui.movies.presenter


import com.pipecode.testsunbelt.R
import com.pipecode.testsunbelt.base.BasePresenter
import com.pipecode.testsunbelt.data.remote.MoviesApi
import com.pipecode.testsunbelt.ui.movies.MovieView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.util.*
import javax.inject.Inject

class MoviesPresenter(movieView: MovieView) : BasePresenter<MovieView>(movieView) {
    @Inject
    lateinit var movieApi: MoviesApi

    private var subscription: Disposable? = null

    override fun onViewCreated() {
        loadMovies(1)
    }

    fun loadMovies(page: Int) {
        view.showLoading()
        subscription = movieApi
            .getMovies(
                "ea98605c5fe61bad588f5ad5b9097365",
                page,
                Locale.getDefault().language
            )
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .doOnTerminate { view.hideLoading() }
            .subscribe(
                { moviesList ->
                    run {
                        view.updateMovies(moviesList.results)
                    }
                },
                { view.showError(R.string.unknown_error) }
            )
    }

    override fun onViewDestroyed() {
        subscription?.dispose()
    }
}