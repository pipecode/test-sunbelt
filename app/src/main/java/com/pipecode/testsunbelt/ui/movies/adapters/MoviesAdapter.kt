package com.pipecode.testsunbelt.ui.movies.adapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.pipecode.testsunbelt.R
import com.pipecode.testsunbelt.data.local.Movie
import com.pipecode.testsunbelt.databinding.ItemMovieBinding
import com.pipecode.testsunbelt.ui.moviedetail.MovieDetailActivity

class MoviesAdapter(private val context: Context) :
    RecyclerView.Adapter<MoviesAdapter.MoviesViewHolder>() {

    private var movies: List<Movie> = listOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MoviesViewHolder {
        val layoutInflater = LayoutInflater.from(context)
        val binding: ItemMovieBinding =
            DataBindingUtil.inflate(layoutInflater, R.layout.item_movie, parent, false)
        return MoviesViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return movies.size
    }

    override fun onBindViewHolder(holder: MoviesViewHolder, position: Int) {
        holder.bind(movies[position])
    }

    fun updateMovies(movies: List<Movie>) {
        this.movies = movies
        notifyDataSetChanged()
    }

    class MoviesViewHolder(private val binding: ItemMovieBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(movie: Movie) {
            binding.movie = movie
            binding.listener = ClickHandler()
            binding.executePendingBindings()
        }
    }
}

class ClickHandler {
    fun onMovieInfoClicked(view: View, movie: Movie) {
        val intent = Intent(view.context, MovieDetailActivity::class.java)
        intent.putExtra("TITLE", movie.title)
        intent.putExtra("OVERVIEW", movie.overview)
        intent.putExtra("BACKDROP", movie.backdrop_path)
        intent.putExtra("VOTE_COUNT", movie.voteCount)
        intent.putExtra("VOTE_AVERAGE", movie.vote_average)
        view.context.startActivity(intent)
    }
}