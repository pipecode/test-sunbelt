package com.pipecode.testsunbelt.ui.moviedetail

import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.pipecode.testsunbelt.R
import com.pipecode.testsunbelt.utils.BASE_URL_IMG
import kotlinx.android.synthetic.main.activity_movie_detail.*
import kotlinx.android.synthetic.main.content_movie_detail.*

class MovieDetailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie_detail)
        setSupportActionBar(toolbar)

        val actionbar = supportActionBar
        actionbar!!.title = intent.getStringExtra("TITLE")
        actionbar.setDisplayHomeAsUpEnabled(true)
        actionbar.setDisplayHomeAsUpEnabled(true)

        fetchExtras()

        fab.setOnClickListener { view ->
            Snackbar.make(view, getString(R.string.todo_favorite_movie), Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
        }

    }

    fun fetchExtras() {
        val backdropImg = intent.getStringExtra("BACKDROP")
        if (backdropImg != null) {
            Glide.with(this).load(BASE_URL_IMG + backdropImg)
                .into(iv_backdrop)
        }
        tv_overview.text = intent.getStringExtra("OVERVIEW")

    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
