package com.pipecode.testsunbelt

import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.pipecode.testsunbelt.base.BaseActivity
import com.pipecode.testsunbelt.data.local.Movie
import com.pipecode.testsunbelt.databinding.ActivityMainBinding
import com.pipecode.testsunbelt.ui.movies.MovieView
import com.pipecode.testsunbelt.ui.movies.adapters.MoviesAdapter
import com.pipecode.testsunbelt.ui.movies.presenter.MoviesPresenter
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity<MoviesPresenter>(), MovieView {

    private lateinit var binding: ActivityMainBinding

    private val moviesAdapter = MoviesAdapter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding.adapter = moviesAdapter
        binding.layoutManager = LinearLayoutManager(this)
        binding.dividerItemDecoration = DividerItemDecoration(this, LinearLayoutManager.VERTICAL)
        presenter.onViewCreated()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onViewDestroyed()
    }

    override fun updateMovies(movies: List<Movie>) {
        moviesAdapter.updateMovies(movies)
    }

    override fun showError(error: String) {
        Toast.makeText(this, error, Toast.LENGTH_LONG).show()
    }

    override fun showLoading() {
        binding.progressVisibility = View.VISIBLE
    }

    override fun hideLoading() {
        binding.progressVisibility = View.GONE
    }

    override fun instantiatePresenter(): MoviesPresenter {
        return MoviesPresenter(this)
    }
}
