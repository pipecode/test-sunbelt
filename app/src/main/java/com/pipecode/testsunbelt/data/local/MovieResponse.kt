package com.pipecode.testsunbelt.data.local

data class MovieResponse(
    val results: List<Movie>,
    val age: Int,
    val totalResults: Int,
    val totalPages: Int
)


