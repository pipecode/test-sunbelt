package com.pipecode.testsunbelt.data.local

data class Movie(
    var popularity: Double,
    var voteCount: Int,
    var video: Boolean,
    var poster_path: String,
    var id: Int,
    var adult: Boolean,
    var backdrop_path: String,
    var originalLanguage: String,
    var originalTitle: String,
    var genreIds: List<Int>,
    var title: String,
    var vote_average: String,
    var overview: String,
    var releaseDate: String

)