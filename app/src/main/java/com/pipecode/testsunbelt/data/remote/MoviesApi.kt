package com.pipecode.testsunbelt.data.remote

import com.pipecode.testsunbelt.data.local.MovieResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface MoviesApi {

    @GET("upcoming")
    fun getMovies(
        @Query("api_key") apiKey: String,
        @Query("page") page: Int,
        @Query("language") language: String
    ): Observable<MovieResponse>
}